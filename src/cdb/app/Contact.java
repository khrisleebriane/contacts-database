package cdb.app;

import java.awt.*;

public class Contact {

	private int id;
	private String name;
	private String address;
	private String contactNumber1;
	private String contactNumber2;
	private String emailAddress;
	private IconShape iconShape;
	private Color iconColor;
	
	public Contact(int id, 
			String name, 
			String address,
			String contactNumber1,
			String contactNumber2,
			String emailAddress,
			IconShape iconShape,
			Color iconColor) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.contactNumber1 = contactNumber1;
		this.contactNumber2 = contactNumber2;
		this.iconShape = iconShape;
		this.iconColor = iconColor;
	}
	
	public Contact(int id) {
		this(id,
			"",
			"",
			"",
			"",
			"",
			IconShape.None,
			Color.white);
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getContactNumber1() {
		return this.contactNumber1;
	}
	
	public void setContactNumber1(String contactNumber) {
		this.contactNumber1 = contactNumber;
	}
	
	public String getContactNumber2() {
		return this.contactNumber2;
	}
	
	public void setContactNumber2(String contactNumber) {
		this.contactNumber2 = contactNumber;
	}
	
	public String getEmailAddress() {
		return this.emailAddress;
	}
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public IconShape getIconShape() {
		return this.iconShape;
	}
	
	public void setIconShape(IconShape shape) {
		this.iconShape = shape;
	}
	
	public Color getIconColor() {
		return this.iconColor;
	}
	
	public void setIconColor(Color color) {
		this.iconColor = color;
	}

}
