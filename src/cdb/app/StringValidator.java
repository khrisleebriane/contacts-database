package cdb.app;

public interface StringValidator {

	boolean isValid(String s);
	
}
