package cdb.app;

public enum IconShape {
	None,
	Circle,
	Oval,
	Square,
	Rectangle
}
