package cdb.app;

import java.util.*;

public class ContactComparator
	implements Comparator<Contact> {

	@Override
	public int compare(Contact contact1, Contact contact2) {
		return Integer.compare(contact1.getId(), contact2.getId());
	}

}
