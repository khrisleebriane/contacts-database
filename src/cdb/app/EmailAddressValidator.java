package cdb.app;

import java.util.regex.*;

public class EmailAddressValidator {
	
	private static String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private Matcher matcher;
	private Pattern pattern;
	
	public EmailAddressValidator() {
		this.pattern = Pattern.compile(regex);
	}
	
	public boolean isValid(String s) {
		matcher = pattern.matcher(s);
		return matcher.matches();
	}
	
}
