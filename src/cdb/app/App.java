package cdb.app;

public class App {

	public static void main(String[] args) {
		ContactsDatabase database = new ContactsDatabase("contacts.txt");
		database.addContact(new Contact(1));
		database.addContact(new Contact(2));
		database.addContact(new Contact(9));
		database.addContact(new Contact(10));
		for (int i = 0; i < 10; i++) {
			Contact contact = new Contact(database.generateContactId());
			database.addContact(contact);
		}
		database.tryCopyToTextFile();
	}
	
}
