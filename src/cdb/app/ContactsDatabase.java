package cdb.app;

import java.io.*;
import java.util.*;

public class ContactsDatabase {

	private ArrayList<Contact> contacts;
	File textFile;
	
	public ContactsDatabase(String fileName) {
		this.contacts = new ArrayList<Contact>();
		this.textFile = new File(fileName);
	}
	
	public ArrayList<Contact> getContacts() {
		return this.contacts;
	}

	public Contact getContactById(int id) {
		Contact result = null;
		for (Contact contact : this.contacts) {
			if (contact.getId() == id) {
				result = contact;
				break;
			}
		}
		return result;
	}
	
	public void addContact(Contact contact) {
		this.contacts.add(contact);
	}
	
	public void deleteContact(Contact contact) {
		this.contacts.remove(contact);
	}
	
	public boolean tryCopyToTextFile() {
		boolean result = true;
		try (PrintWriter writer = new PrintWriter(new FileWriter(textFile))){
			for (Contact contact : this.contacts) {
				writer.format("%d", contact.getId());
				writer.println();
			}
		} 
		catch (IOException e) {
			result = false;
		}
		return result;
	}
	
	public int generateContactId() {		
		int result = 1;
		contacts.sort(new ContactComparator());
		if (contacts.size() > 0) {
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (int index = 0; index < contacts.size(); index++) {
				ids.add(contacts.get(index).getId());
			}
			for (int id : ids) {
				result = id + 1;
				if (!ids.contains(result)) {
					break;
				}
			}
		}
		return result;
	}
	
}
